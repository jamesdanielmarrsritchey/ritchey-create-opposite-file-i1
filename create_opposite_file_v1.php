<?php
#Name:Create Opposite File v1
#Description:Convert a file to a Ritchey Opposite Data encoded opposite file. Returns "TRUE" on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. It is suggested to use ".opp" as the file extension.
#Arguments:'file' (required) is a file path for the file to convert. 'destination' is a file path for where to save the opposite file to. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):file:file:required,destination:path:required,display_errors:bool:optional
#Content:
if (function_exists('create_opposite_file_v1') === FALSE){
function create_opposite_file_v1($file, $destination, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@is_file($file) === FALSE){
		$errors[] = 'file';
	}
	if (@is_dir(dirname($destination)) === FALSE){
		$errors[] = 'destination';
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert file data to Ritchey Opposite Data, and save to file]
	if (@empty($errors) === TRUE){
		###If $destination exists, delete it so that data "overwrites" the file instead of appending to the end of it.
		if (@is_file($destination) === TRUE){
			unset($destination);
		}
		###Read file 3 bytes at a time (3 bytes at a time ensures base64 compatibility as it creates the same result as if you had converted the entire file to base64 at once)
		$handle = @fopen($file,"r");
		while(! feof($handle)){
			$data = @fread($handle,"3");
			####Convert bytes to base64
			$data = @base64_encode($data);
			####Convert base64 to Ritchey Opposite Data (A-Za-z0-9+/ becomes /+9-0z-aZ-A, but = remains =.)
			$data = @str_split($data, 1);
			foreach ($data as &$item) {
				if ($item === 'A'){
					$item = '/';
				} else if ($item === 'B'){
					$item = '+';
				} else if ($item === 'C'){
					$item = '9';
				} else if ($item === 'D'){
					$item = '8';
				} else if ($item === 'E'){
					$item = '7';
				} else if ($item === 'F'){
					$item = '6';
				} else if ($item === 'G'){
					$item = '5';
				} else if ($item === 'H'){
					$item = '4';
				} else if ($item === 'I'){
					$item = '3';
				} else if ($item === 'J'){
					$item = '2';
				} else if ($item === 'K'){
					$item = '1';
				} else if ($item === 'L'){
					$item = '0';
				} else if ($item === 'M'){
					$item = 'z';
				} else if ($item === 'N'){
					$item = 'y';
				} else if ($item === 'O'){
					$item = 'x';
				} else if ($item === 'P'){
					$item = 'w';
				} else if ($item === 'Q'){
					$item = 'v';
				} else if ($item === 'R'){
					$item = 'u';
				} else if ($item === 'S'){
					$item = 't';
				} else if ($item === 'T'){
					$item = 's';
				} else if ($item === 'U'){
					$item = 'r';
				} else if ($item === 'V'){
					$item = 'q';
				} else if ($item === 'W'){
					$item = 'p';
				} else if ($item === 'X'){
					$item = 'o';
				} else if ($item === 'Y'){
					$item = 'n';
				} else if ($item === 'Z'){
					$item = 'm';
				} else if ($item === 'a'){
					$item = 'l';
				} else if ($item === 'b'){
					$item = 'k';
				} else if ($item === 'c'){
					$item = 'j';
				} else if ($item === 'd'){
					$item = 'i';
				} else if ($item === 'e'){
					$item = 'h';
				} else if ($item === 'f'){
					$item = 'g';
				} else if ($item === 'g'){
					$item = 'f';
				} else if ($item === 'h'){
					$item = 'e';
				} else if ($item === 'i'){
					$item = 'd';
				} else if ($item === 'j'){
					$item = 'c';
				} else if ($item === 'k'){
					$item = 'b';
				} else if ($item === 'l'){
					$item = 'a';
				} else if ($item === 'm'){
					$item = 'Z';
				} else if ($item === 'n'){
					$item = 'Y';
				} else if ($item === 'o'){
					$item = 'X';
				} else if ($item === 'p'){
					$item = 'W';
				} else if ($item === 'q'){
					$item = 'V';
				} else if ($item === 'r'){
					$item = 'U';
				} else if ($item === 's'){
					$item = 'T';
				} else if ($item === 't'){
					$item = 'S';
				} else if ($item === 'u'){
					$item = 'R';
				} else if ($item === 'v'){
					$item = 'Q';
				} else if ($item === 'w'){
					$item = 'P';
				} else if ($item === 'x'){
					$item = 'O';
				} else if ($item === 'y'){
					$item = 'N';
				} else if ($item === 'z'){
					$item = 'M';
				} else if ($item === '0'){
					$item = 'L';
				} else if ($item === '1'){
					$item = 'K';
				} else if ($item === '2'){
					$item = 'J';
				} else if ($item === '3'){
					$item = 'I';
				} else if ($item === '4'){
					$item = 'H';
				} else if ($item === '5'){
					$item = 'G';
				} else if ($item === '6'){
					$item = 'F';
				} else if ($item === '7'){
					$item = 'E';
				} else if ($item === '8'){
					$item = 'D';
				} else if ($item === '9'){
					$item = 'C';
				} else if ($item === '+'){
					$item = 'B';
				} else if ($item === '/'){
					$item = 'A';
				} else if ($item === '='){
					#Do nothing
				} else {
					$errors[] = "ritchey opposite data";
					goto result;
				}
			}
			$data = @implode($data);
			####Base64 decode data
			$data = base64_decode($data);
			####Write Ritchey Opposite Data to file
			file_put_contents($destination, $data, FILE_APPEND | LOCK_EX);
		}
		@fclose($handle);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('create_opposite_file_v1_format_error') === FALSE){
			function create_opposite_file_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("create_opposite_file_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>